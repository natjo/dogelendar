function saveCalendarState(calendarState) {
    localStorage.setItem('dogelendar_calendarState', JSON.stringify(calendarState));
}

function loadCalendarState() {
    let calendarState = JSON.parse(localStorage.getItem('dogelendar_calendarState'));
    if (calendarState == null){
        calendarState = createNewCalendarState();
    }
    return calendarState;
}

function deleteCalendarState() {
    localStorage.clear();
}

function createNewCalendarState(){
    let calendarState = {};
    for(let i=1;i<=24;i++){
        let day = `door_${i}`;
        calendarState[day] = 'calendar-door-closed';
    }
    return calendarState;
}
